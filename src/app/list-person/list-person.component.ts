import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'list-person',
  templateUrl: './list-person.component.html',
  styleUrls: ['./list-person.component.css']
})
export class ListPersonComponent implements OnInit {

	arrPerson = [
		{name: 'Vu Hung Cuong', age: 18},
		{name: 'Tap Can Binh', age: 60},
		{name: 'Ly Khac Cuong', age: 57},
		{name: 'Nguyen Tan Dung', age: 58}
	];

  constructor() { }

  ngOnInit() {
  }

}
