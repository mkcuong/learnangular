import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

	name: string = '';
	isHiglight = false;

  constructor() { }

  ngOnInit() {
  }

  // showEvent(event) {
  // 	this.name = event.target.value;
  // }

}
