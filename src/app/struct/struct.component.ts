import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'struct',
  templateUrl: './struct.component.html',
  styleUrls: ['./struct.component.css']
})
export class StructComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  myArr = ['a', 'b', 'c', 'e', 'f'];
}
