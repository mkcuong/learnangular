import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IpAComponent } from './ip-a.component';

describe('IpAComponent', () => {
  let component: IpAComponent;
  let fixture: ComponentFixture<IpAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IpAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IpAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
