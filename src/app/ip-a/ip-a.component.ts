import { Component, OnInit } from '@angular/core';
import { IpService } from '../ip.service';

@Component({
  selector: 'ip-a',
  templateUrl: './ip-a.component.html',
  styleUrls: ['./ip-a.component.css']
})
export class IpAComponent implements OnInit {

	ip: string;

  constructor(private ipService: IpService) { 
  	
  }

  ngOnInit() {
    this.ipService.getIp()
      .then(ip => this.ip = ip)
      .catch(err => console.log(err));
  }


}
