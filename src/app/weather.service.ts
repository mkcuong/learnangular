import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class WeatherService {
	constructor(private http: Http) {

	}

	getTemp(cityName: string) {
		const url = "http://api.openweathermap.org/data/2.5/weather?appid=35ad0093a499044968b2f4a00a227294&units=metri&q=" + cityName;
		return this.http.get(url)
			.toPromise()
			.then(res => res.json());
	}
}
