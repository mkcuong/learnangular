import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'learn-pipe',
  templateUrl: './learn-pipe.component.html',
  styleUrls: ['./learn-pipe.component.css']
})
export class LearnPipeComponent implements OnInit {

	birthday = new Date(2017, 10, 27);
	person = { id: 1, name: 'Vu Hung Cuong', age: 23 };

  constructor() { }

  ngOnInit() {
  }

}
