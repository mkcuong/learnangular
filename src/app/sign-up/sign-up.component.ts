import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
	formSignUp: FormGroup;

  constructor() {
  	this.formSignUp = new FormGroup({
  		email: new FormControl(),
  		password: new FormControl(),
  		subject: new FormGroup({
  			nodeJs: new FormControl(),
  			angular: new FormControl(),
  			react: new FormControl(),
  		})
  	});
  }

  onSubmit() {
  	console.log(this.formSignUp.value);
  }

  ngOnInit() {
  }

}
