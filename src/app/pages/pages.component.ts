import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.css']
})
export class PagesComponent implements OnInit {

	name: string = '';
	year: number = null;
	display: string = '';
	status: boolean = true;
	FilterStatus: string = 'XEM_TAT_CAT';

	arrPhone = [
		{id: 1, name: 'Galaxy 7', year: 2016, display: 'LED-backlit IPS LCD, capacitive touchscreen, 16M colors', status: true },
		{id: 2, name: 'Galaxy 6', year: 2015, display: 'LED-backlit IPS LCD, capacitive touchscreen, 16M colors', status: true },
		{id: 3, name: 'Galaxy 5', year: 2012, display: 'LED-backlit IPS LCD, capacitive touchscreen, 16M colors', status: false },
		{id: 4, name: 'Iphone 4', year: 2013, display: 'LED-backlit IPS LCD, capacitive touchscreen, 16M colors', status: true },
		{id: 5, name: 'Galaxy 3', year: 2013, display: 'LED-backlit IPS LCD, capacitive touchscreen, 16M colors', status: false }
	]

  constructor() { }

  ngOnInit() {
  }

  Addphone() {
  	let n = this.arrPhone.length - 1;
  	this.arrPhone.unshift({
  		id: n + 1,
  		name: this.name,
  		year: this.year,
  		display: this.display,
  		status: this.status
  	});
  }

  showStatus( status: boolean ) {
  	const dkXEMTATCA = this.FilterStatus === 'XEM_TAT_CAT';
  	const dkXEMHETHANG = this.FilterStatus === 'HET_HANG' && !status;
  	const dkXEMCONHANG = this.FilterStatus === 'CON_HANG' && status;
  	return dkXEMTATCA || dkXEMCONHANG || dkXEMHETHANG;
  }

  Delete( id: number ) {
  	const index = this.arrPhone.findIndex(phone => phone.id === id);
  	this.arrPhone.splice(index, 1);
  }

  Status( id: number ) {
  	const index = this.arrPhone.findIndex(phone => phone.id === id);
  	this.arrPhone[index].status = !this.arrPhone[index].status;
  }
}
