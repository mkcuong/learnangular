import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit {

	value: number = 0;

  constructor() { }

  ngOnInit() {
  }

  changeValue(isAdd: boolean) {
  	if (isAdd) {
  		++ this.value ;
  	} else {
  		-- this.value ;

  	}
  }

}
