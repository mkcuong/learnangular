import { Component, OnInit } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Component({
	template: `
	<form (ngSubmit)="onSubmit(formSignIn);" #formSignIn="ngForm">
		<input 
						placeholder="Email" 
						ngModel
						#txtEmail="ngModel" 	
						name='email' 
						required
						email
		>
		<p *ngIf="txtEmail.touched && txtEmail.errors?.required">
			Email is required
		</p>
		<p *ngIf="txtEmail.touched && txtEmail.errors?.email">
			Email is not valid
		</p>
		<br><br>
		<input 
						type="password" 
						placeholder="Password" 
						ngModel 
						#txtPassword="ngModel"
						name='password' 
						required
						minlength="6"
						pattern="[a-z]*"
		>
		<br><br>
		<div ngModelGroup="sub">
			<label>Nodejs </label>
			<input type="checkbox" ngModel name="Nodejs">
			<label>React </label>
			<input type="checkbox" ngModel name="React">
			<label>Angular </label>
			<input type="checkbox" ngModel name="Angular">
		</div>
		<button [disabled]="formSignIn.invalid">Submit</button>
	</form>
	<button (click)="postToExpress()">POST</button>
	<p>{{ txtEmail.errors | json }}</p>
	<p>{{ txtPassword.errors | json }}</p>
	`,
	selector: 'sign-in'
})

export class SignInComponent implements OnInit {
	
	constructor(private http: Http) {

	}

	ngOnInit() {

	}

	onSubmit(formSignIn) {
		console.log(formSignIn.value);
	}

	postToExpress() {
		const url = 'http://localhost:3000/signin';
		const headers = new Headers({ 'Content-Type': 'application/json' });
		const body = JSON.stringify({name: 'KhoaPham'})
		this.http.post(url, body, {headers})
			.toPromise()
			.then(res => res.text())
			.then(resText => console.log(resText));
	}
}