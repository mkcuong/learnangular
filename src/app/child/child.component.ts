import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {

	@Output() myclick = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() {
  }

  AddClick() {
  	this.myclick.emit(true);
  }

  SubClick() {
  	this.myclick.emit(false);
  }



}
