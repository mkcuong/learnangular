import { Component, OnInit, ViewChild } from '@angular/core';

import { ZzzComponent } from '../zzz/zzz.component';

@Component({
  selector: 'yyy',
  templateUrl: './yyy.component.html',
  styleUrls: ['./yyy.component.css']
})
export class YyyComponent implements OnInit {

	@ViewChild(ZzzComponent)
	myZZZ: ZzzComponent

  constructor() { }

  ngOnInit() {
  }

  onAdd() {
  	this.myZZZ.value ++;
  }

  onSub() {
  	this.myZZZ.value --;
  }

}
