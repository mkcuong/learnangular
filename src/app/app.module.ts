import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { WordComponent } from './word/word.component';
import { BookComponent } from './book/book.component';
import { UserComponent } from './user/user.component';
import { StructComponent } from './struct/struct.component';
import { WordsComponent } from './words/words.component';
import { PersonComponent } from './person/person.component';
import { ListPersonComponent } from './list-person/list-person.component';
import { PagesComponent } from './pages/pages.component';
import { ParentComponent } from './parent/parent.component';
import { ChildComponent } from './child/child.component';
import { XxxComponent } from './xxx/xxx.component';
import { YyyComponent } from './yyy/yyy.component';
import { ZzzComponent } from './zzz/zzz.component';
import { CardComponent } from './card/card.component';
import { LearnPipeComponent } from './learn-pipe/learn-pipe.component';
import { IpComponent } from './ip/ip.component';
import { SignInComponent } from './sign-in.component';


import { HttpModule } from '@angular/http';
import { IpAComponent } from './ip-a/ip-a.component';
import { IpService } from './ip.service';
import { WeatherComponent } from './weather/weather.component';
import { SignUpComponent } from './sign-up/sign-up.component';

@NgModule({
  declarations: [
    AppComponent,
		WordComponent,
		BookComponent,
		UserComponent,
		StructComponent,
		WordsComponent,
		PersonComponent,
		ListPersonComponent,
		PagesComponent,
		ParentComponent,
		ChildComponent,
		XxxComponent,
		YyyComponent,
		ZzzComponent,
		CardComponent,
		LearnPipeComponent,
		IpComponent,
		IpAComponent,
		WeatherComponent,
		SignInComponent,
		SignUpComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule, 
    ReactiveFormsModule
  ],
  providers: [IpService],
  bootstrap: [AppComponent]
})
export class AppModule { }
